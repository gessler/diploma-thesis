#!/bin/bash
#PBS -N job.name
#PBS -l nodes=n:ppn=n
#PBS -l walltime=dd:hh:mm:ss
#PBS -l mem=n[mb/gb]

## check if $SCRIPT_FLAGS is "set"
if [ -n "${SCRIPT_FLAGS}" ] ; then
   ## but if positional parameters are already present
   ## we are going to ignore $SCRIPT_FLAGS
   if [ -z "${*}"  ] ; then
      set -- ${SCRIPT_FLAGS}
   fi
fi
