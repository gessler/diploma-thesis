#!/usr/bin/perl
use strict;
use warnings;

use Getopt::Long;
use Bio::SeqIO;
use Bio::Coordinate::Pair;
use Bio::DB::Fasta;

my ($help,$gff,$fasta,$embl);

my $USAGE = "$0 -h|--help || --gff input_gff --fasta input_fasta --embl output_embl\n";

my $result = GetOptions(        'h|help'        => \$help,
                                'gff=s'         => \$gff,
				'fasta=s'	=> \$fasta,
				'embl=s'	=> \$embl,
                        );

die $USAGE if $help;
die $USAGE unless ($gff and $fasta and $embl);

my $seqdb = Bio::DB::Fasta->new("$fasta");
my $seq_out = Bio::SeqIO->new(-file => ">$embl", '-format' => 'embl');

open (F,$gff);
my $primary;
my $primary_loc;
my @mirnas;
my $pri_on_ref;
my $pri_as_ref;
while (my $f=<F>){
	chomp $f;
	next if ($f=~/^#/);
	my @a = split("\t",$f);
	my $ref_id = $a[0];
	my $abs_start = $a[3];
	my $abs_end = $a[4];
	my $strand = $a[6];
	$strand = join("",$a[6],1);
	my ($id) = ($a[8]=~/ID=([^;]+)(;|$)/);
	my $evidence = $a[1];
	my $seq_in = $seqdb->get_Seq_by_id($ref_id);
	my $subseq = $seq_in->subseq($abs_start => $abs_end);
	if ($a[2]=~/^miRNA_primary_transcript$/){
		unless (@mirnas==0){
			foreach my $mirna (@mirnas){
				$primary->add_SeqFeature($mirna);
			}
			$seq_out->write_seq($primary);
		}
		undef @mirnas;
		$pri_as_ref = Bio::Location::Simple->new( -seq_id => $id, -start => 1, -end => ($abs_end-$abs_start+1), -strand => +1 );
		$pri_on_ref = Bio::Location::Simple->new( -seq_id => $ref_id, -start => $abs_start, -end => $abs_end, -strand => $strand );
		my $on_ref = Bio::SeqFeature::Generic->new( -seq_id => $ref_id, -primary_tag => "source", -start => 1, -end => ($abs_end-$abs_start+1), -tag => { accession => $id, source_start => $abs_start, source_end => $abs_end, source_strand => $strand, source_seq_id => $ref_id,});
		$primary = Bio::Seq::RichSeq->new( -seq => $subseq, -id  => $id, -accession_number => $id,);
		$primary->add_SeqFeature($on_ref);
	}
	if ($a[2]=~/^miRNA$/){
		my $ref_to_pri = Bio::Coordinate::Pair->new( -in  => $pri_on_ref, -out => $pri_as_ref);
		my $mir_on_ref = Bio::Location::Simple->new( -seq_id => $ref_id, -start => $abs_start, -end => $abs_end, -strand => $strand );
		my $mir_on_pri = $ref_to_pri->map( $mir_on_ref );
		my $mir = Bio::SeqFeature::Generic->new(-display_name=>$id, -primary_tag=>"miRNA", -start=>$mir_on_pri->start, -end=>$mir_on_pri->end, -tag => { accession => $id, evidence=>$evidence,});
		push (@mirnas,$mir);
	}
}
unless (@mirnas==0){
	foreach my $mirna (@mirnas){
		$primary->add_SeqFeature($mirna);
	}
	$seq_out->write_seq($primary);
}
close F;
