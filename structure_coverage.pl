#!/usr/bin/perl 
use strict;
use warnings;
use Bio::DB::Sam;
use Bio::DB::SeqFeature;
use Bio::SeqIO;
use Getopt::Long;
use Math::Round;
use Cwd;
use File::Temp qw/tempfile/;

my $USAGE		= "$0 -h | [-f FASTA_FILE -b BAM_FILE|-r reference_miRNA_FASTA] [-NH MAX_NUM_HITS] [-NM MAX_NUM_MISMATCHES] [-min MIN_TIMES_SEQUENCED] [-t type] [--host host --user user --passwd passwd] [-o outpath] [-a] [--struct structure_PSfile] [--rnafold RNAfold_binary] [-l libsize] [--verbose]  REF:START..STOP|ID [...]\n";

my ($help, $fasta, $bam,$NM,$NH,$min,@types,$host,$dbname,$user,$passwd,$aliases,$outpath,$PSfile,$RNAfold_bin,$libsize, $verbose, $refseq);

#default settings
$RNAfold_bin	= "";
@types			= ("");
$host			= "";
$user			= "";
$passwd			= "";
$dbname			= '';
$outpath		= "";

my $PS_code=<<_E_O_F_;
/range 0.8 def
/drawcoverage {
  /Smax SMAX def
  0
  coor {
    aload pop
    S 3 index get
    RANGE 
	invert {dup 0 eq {range exch sub 0}{range exch sub 1} ifelse} {1} ifelse
    4 index 0 eq {0 sethsbcolor}{4 index S length 1 sub eq{0.5 sethsbcolor}{1 sethsbcolor} ifelse} ifelse
    %1 sethsbcolor
    newpath
    fsize 2 div 0 360 arc
    fill
    1 add
  } forall
} bind def
/colorbar { % xloc yloc colorbar -> []
  /STR 8 string def
  gsave
    xmin xmax add size sub 2 div
    ymin ymax add size sub 2 div translate
    size dup scale
    translate
    0.015 dup scale
    /tics 64 def
    gsave
      10 tics div 1 scale
      1 1 tics
      {
	  dup 0 moveto 0.5 add
	  tics div range mul
	  invert {range exch sub} if
	  1 1 sethsbcolor
	  1 0 rlineto 0 1 rlineto -1 0 rlineto closepath fill
      } for
    grestore
    0 setgray
    -0.1 1.01 moveto (>0) gsave 0.1 dup scale show grestore
    3.5 -2.01 moveto (TYPE) gsave 0.1 dup scale show grestore
    10 1.01 moveto Smax STR cvs
    gsave 0.1 dup scale dup stringwidth pop -2 div 0 rmoveto show grestore
  grestore
} bind def
_E_O_F_

my $result = GetOptions(
	"h|help" 	=> \$help,
	"host=s"	=> \$host,
	"dbname=s"	=> \$dbname,
	"user=s"	=> \$user,
	"passwd=s"	=> \$passwd,
	"fasta=s"	=> \$fasta,
	"bam=s"		=> \$bam,
	"NM=i"		=> \$NM,
	"NH=i"		=> \$NH,
	"min=i"		=> \$min,
	"type=s"	=> \@types,
	"aliases"	=> \$aliases,
	'outpath=s'	=> \$outpath,
	'struct=s'	=> \$PSfile,
	'rnafold=s'	=> \$RNAfold_bin,
	'libsize=i'	=> \$libsize,
	'verbose'	=>  \$verbose,
	'refseq=s'	=> \$refseq,
);

my (%kids);

parse_types(\@types,\%kids);

die $USAGE if $help; 
die "No input!\n\n". $USAGE unless ($bam && $fasta && @ARGV) || ($refseq && -f $refseq);

my $db      	= Bio::DB::SeqFeature::Store->new( 
	-adaptor	=> 'DBI::mysql',
	-dsn   		=> sprintf('dbi:mysql:database=%s;host=%s;user=%s;password=%s',$dbname, $host,$user,$passwd),
	-write   	=> 0 
);

my $sam;
my $IO;
my $BAMNAME;
if ($bam) {
	$BAMNAME		= `basename $bam`;
	chomp($BAMNAME);
	$BAMNAME		=~ s/\.bam$//;
	$sam 			= Bio::DB::Sam->new(-bam  =>$bam, -fasta=>$fasta) or die $!;
}
else {
	$IO 			= Bio::SeqIO->new(-file=>$refseq,-format=>"fasta");
	$BAMNAME		= `basename $refseq`;
	chomp($BAMNAME);
	$BAMNAME		=~ s/\.fas?t?a?$//;
}

print STDERR "Output path: $outpath. Using Bam file $bam" . ($libsize ? " with TP2M normalization of libsize $libsize " : "with MAX normalization of raw counts") ."\n"  if $verbose && $sam;

my $range	=  "range mul";
$range		=  "Smax div range mul" unless $libsize;
$PS_code	=~ s/RANGE/$range/;

$range		= $libsize ? "TP2M" : "nreads";
$PS_code	=~ s/TYPE/$range/;

my $done;

foreach my $I (@ARGV) {
	my @features;

	if ($I =~ /^([^:]+):(\d+)(\.{2,3}|-)(\d+)$/) {
		@features 	= $db->get_features_by_location($1,$2,$4);
		@features	= grep {my $F=$_; grep { $F->type eq $_ } @types;} @features;
	}
	else {
		@features	= $aliases ? $db->get_features_by_alias(-name=>$I) : $db->get_features_by_name(-name=>$I);
		@features	= grep {my $F=$_; grep { $F->type eq $_ } @types;} @features;
	}

	foreach my $f (@features) {
		next unless grep {$f->type eq $_} @types;
		my @locs;
		if (exists $kids{$f->type}) {
			@locs	= sort {$f->strand > 0 ? $a->start <=> $b->start : $b->start <=> $a->start} $f->get_SeqFeatures($kids{$f->type});
		}
		else {
			@locs	= $f;
		}
		my ($s,@cov,%COV);
		foreach my $l (@locs) {
			$s		.= $l->seq->seq;
			if ($sam) { #coverage
				unless ($NH || $NM || $min) {
					my ($cv) = $sam->features(-type=>"coverage",-seq_id=>$l->seq_id,-start=>$l->start,-end=>$l->end);
					push @cov, $f->strand < 0 ? reverse $cv->coverage : $cv->coverage;
				}
				else {
					my $callback = sub {
						my($seqid,$pos,$pileup) = @_;
						READ: for my $p (@$pileup) {
							my $alignment = $p->alignment;
							my $wrapper   = Bio::DB::Bam::AlignWrapper->new($alignment,$sam);

							my ($nh)	= $wrapper->get_tag_values("NH") if $wrapper->has_tag("NH");
							next if $NH && $NH< $nh;

							my ($nm)	= $wrapper->get_tag_values("NM") if $wrapper->has_tag("NM");
							next if $NM && $NM< $nm;
							$COV{$pos}++;
						}
					};
					$sam->fast_pileup($l->seq_id.":".$l->start.'..'.$l->end,$callback,1);
					my (@COV);
					for (my $i=$l->start;$i<=$l->end;$i++) {
						push @COV, $COV{$i}? ($min && $COV{$i}<$min ? 0 : $COV{$i}) : 0;
					}
					push @cov, $f->strand < 0 ? reverse @COV : @COV;
				}
			}
		}
		if ($IO) {# positions of reference sequence
			while (my $mir = $IO->next_seq) {
				my $r	= $mir->seq;
				$r	=~ tr/Uu/Tt/;
				my $rev	= $mir->revcom->seq;
				$rev	=~ tr/Uu/Tt/; 
				my $w;
				($w)	= ($mir->description =~ /^([0-9\.e\-\+]+)/);
				$w		= 1 unless $w;
				$r= $rev if $s=~ /$rev/i && $s!~ /$r/i;
				while ($s=~ /($r)/gi) {
					my ($start,$end) = ($-[0],$+[0]);
					for (my $i=0; $i<length($s); $i++) {
						if ($i>=$start && $i<=$end) {
							$cov[$i]+=$w*1;
						}
						elsif (! defined $cov[$i]) {
							$cov[$i]=0;
						}
					}
				}
			}
		}
		@cov=TP2M(@cov) if $libsize && $sam;

		warn "Infered coverage for ". scalar @cov." bp of transcript ". $f->name."\n" if $verbose;

		#get max score
		my $max = 0;
		foreach (@cov) {
			$max=$_ if $_>$max;
		}

		my $clean	= clean($f->name);
		my $io		= Bio::SeqIO->new(-file=>">$outpath/$clean.transcript.fasta",-format=>"fasta");
		$io->write_seq(Bio::Seq->new(-display_id=>$clean, -seq=>$s));

		unless ($PSfile && -f $PSfile) {
			my $pwd = getcwd;
			print STDERR "Startin RNAfold..." if $verbose; 
			chdir($outpath);
			system("$RNAfold_bin < $clean.transcript.fasta > $clean.dbn");
			chdir($pwd) or die $!;
			$PSfile="$outpath/${clean}_ss.ps";
			print STDERR `pwd`;
			die "No RNAfold result $PSfile\n" unless -f $PSfile; 
			warn "Finished!\n" if $verbose;
		}
		if (grep {$_>0} @cov) {
			add_coverage2PS($PSfile,$max,@cov);
		}
		else {
			warn "No reads overlapping ". $f->name," in ".(defined $bam ? $bam : $refseq). "\n";
		}
		$done++;
	}
}

warn "Your query ". join("," , @ARGV). " yielded no matches\n" unless $done;

sub TP2M {
	my @l=@_;
	return(map {$_*2e+6/$libsize} @l);
}

sub parse_types {
	my ($types,$kids)= @_;
	for (my $i=0;$i<@$types;$i++) {
		if ($types[$i]=~ /^([^:]+:[^:]+):([^:]+)$/) {
			$kids{$1}=$2;
			$types[$i]=~ s/:[^:]+$//;
		}
	}
}
sub clean {
	my ($t)= @_;
	$t=~ s/[^A-z0-9\-_.]+//g;
	return $t;
}
sub add_coverage2PS {
	my ($PSfile,$Smax,@sp)	= @_;
	open(F, $PSfile) or die "Cannot open $PSfile for parsing! $!\n";
	my $outPSfile			= $PSfile;
	$outPSfile				=~ s/\_ss.ps/.$BAMNAME.coverage.ps/; 
	open(O, ">$outPSfile") or die "Cannot open $outPSfile for parsing! $!\n";
	while (<F>) {
		if (/^drawoutline/) {
			my $PS_code1=$PS_code;
			$Smax=Math::Round::round($Smax);
			$PS_code1=~ s/SMAX/$Smax/;
			print O $PS_code1; 
			print O "/S [\n";
			foreach (@sp) {
				printf O "  %7.5f\n", $_;
			}
			print O "] def\n\n";
			print O "/invert true def\n";
			print O "drawcoverage\n";
			print O "0.1 0.1 colorbar\n";

		}
	print O ;
	}
}
