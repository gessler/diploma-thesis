#!/usr/bin/perl 

use strict;
use warnings;

use Bio::SeqIO;
use Bio::Seq;
use Bio::Factory::EMBOSS;
use Bio::Range;
use Bio::Coordinate::Pair;
use File::Temp qw (tempfile);
use List::Util qw(shuffle);
use Getopt::Long;
use File::Basename;

my ($help,$dat,$window,$training,$outdir);

my $USAGE = "$0 -h|--help || --dat EBML_file [--window i_bp] [--training i_randomSequences] --out output_path\n";

my $result = GetOptions(	'h|help'    	=> \$help,
                		'dat=s' 	=> \$dat,
                		'training=i' 	=> \$training,
				'out=s'		=> \$outdir,
                	);

die $USAGE if $help;
die $USAGE unless ($dat and $outdir);

my @dinuc	= qw(AA      AC      AG      AT      CA      CC      CG      CT      GA      GC      GG      GT      TA      TC      TG      TT);
my @trinuc	= qw(AAA     AAC     AAG     AAT     ACA     ACC     ACG     ACT     AGA     AGC     AGG     AGT     ATA     ATC     ATG     ATT     CAA     CAC     CAG     CAT     CCA     CCC     CCG     CCT     CGA     CGC     CGG     CGT     CTA     CTC     CTG     CTT     GAA     GAC     GAG     GAT     GCA     GCC     GCG     GCT     GGA     GGC     GGG     GGT     GTA     GTC     GTG     GTT     TAA     TAC     TAG     TAT     TCA     TCC     TCG     TCT     TGA     TGC     TGG     TGT     TTA     TTC     TTG     TTT);

my $i          = Bio::SeqIO->new(-format=>"embl",-file=>$dat);
my $factory	= Bio::Factory::EMBOSS->new();
my $compseq	= $factory->program("compseq");

my $filename	= basename($dat);

my ($TIMES);
$TIMES		= $training if (defined $training);
$TIMES		= 2 unless $TIMES;

my $tempdir = File::Temp->newdir();

open (T,">$outdir/$filename.arff") or die "Cannot create output file \"$outdir/$filename.arff\". $!\n";

# print comment,relation, identifiers
print T "%Training set for miRNA validation\n\@Relation mirna\n\@attribute identifier string\n";
# print bracket_one
for (my $i=1; $i<=50; $i++){
	print T "\@attribute bracket_one_$i {LB,RB,DT}\n";
}
# print bracket_fraction_one
print T "\@attribute bracket_fraction_dot_one numeric\n\@attribute bracket_fraction_left_one numeric\n\@attribute bracket_fraction_right_one numeric\n\@attribute bracket_fraction_na_one numeric\n";
# print bracket_two
for (my $i=1; $i<=50; $i++){
	print T "\@attribute bracket_two_$i {LB,RB,DT}\n";
}
# print bracket_fraction_two
print T "\@attribute bracket_fraction_dot_two numeric\n\@attribute bracket_fraction_left_two numeric\n\@attribute bracket_fraction_right_two numeric\n\@attribute bracket_fraction_na_two numeric\n";
# print bases_one
for (my $i=1; $i<=50; $i++){
	print T "\@attribute base_one_$i {G,A,T,C,N,U,R,Y,S,W,K,M,B,D,H,V,.,-}\n";
}
# print base_fraction_one
print T "\@attribute base_fraction_A_one numeric\n\@attribute base_fraction_T_one numeric\n\@attribute base_fraction_G_one numeric\n\@attribute base_fraction_C_one numeric\n\@attribute base_fraction_N_one numeric\n\@attribute base_fraction_na_one numeric\n";
# print bases_two
for (my $i=1; $i<=50; $i++){
	print T "\@attribute base_two_$i {G,A,T,C,N,U,R,Y,S,W,K,M,B,D,H,V,.,-}\n";
}
# print base_fraction_two
print T "\@attribute base_fraction_A_two numeric\n\@attribute base_fraction_T_two numeric\n\@attribute base_fraction_G_two numeric\n\@attribute base_fraction_C_two numeric\n\@attribute base_fraction_N_two numeric\n\@attribute base_fraction_na_two numeric\n";
# print bracket_fraction_loop
print T "\@attribute bracket_fraction_dot_loop numeric\n\@attribute bracket_fraction_left_loop numeric\n\@attribute bracket_fraction_right_loop numeric\n\@attribute bracket_fraction_na_loop numeric\n";
# print base_fraction_loop
print T "\@attribute base_fraction_A_loop numeric\n\@attribute base_fraction_T_loop numeric\n\@attribute base_fraction_G_loop numeric\n\@attribute base_fraction_C_loop numeric\n\@attribute base_fraction_N_loop numeric\n\@attribute base_fraction_na_loop numeric\n";
# print dinucleotide frequency/ratio and trinucleotide frequency/ratio for one
print T join("\n",map({"\@attribute ${_}.freq_one numeric"}@dinuc),map({"\@attribute ${_}.ratio_one numeric"}@dinuc), map({"\@attribute ${_}.freq_one numeric"}@trinuc),map({"\@attribute ${_}.ratio_one numeric"}@trinuc)),"\n";
# print dinucleotide frequency/ratio and trinucleotide frequency/ratio for two
print T join("\n",map({"\@attribute ${_}.freq_two numeric"}@dinuc),map({"\@attribute ${_}.ratio_two numeric"}@dinuc), map({"\@attribute ${_}.freq_two numeric"}@trinuc),map({"\@attribute ${_}.ratio_two numeric"}@trinuc)),"\n";
# print energy
print T "\@attribute free_energy numeric\n";
# print state lucky_shot
print T "\@attribute lucky_shot {Y,N}\n";
# print class field
print T "\@attribute is_miRNA_duplex {Y,N}\n";
print T "\@data\n";

open (B,">$outdir/$filename.csv") or die "Cannot create output file \"$outdir/$filename.csv\". $!\n";
open (G,">$outdir/$filename.gff3") or die "Cannot create output file \"$outdir/$filename.gff3\". $!\n" unless (defined $training);

while (my $s=$i->next_seq) { 
	my $length=$s->length();
	my $dna = $s;
	$dna 	= $s->rev_transcribe if ($s->alphabet eq "rna");
	$s->seq($dna->seq);
	my $sequence=$s->seq(); #keep RNA sequence for folding
	my ($randfile2, $randfile3, $randseqs) = compSEQ_random($s,$TIMES); #if (defined $training);
	my ($fold,$energy) = RNAfold($sequence);
	my ($matrix) = RNAeval($sequence,$fold);
	my ($source) = grep {$_->primary_tag eq "source"} $s->get_SeqFeatures;
	my (@mirnas) = grep {$_->primary_tag eq "miRNA"} $s->get_SeqFeatures;
	my (%seen,%pairs,%seqs,$mb_ref);
	my $lucky="N";
	my $count=1;
	foreach my $ma (sort { $a->start <=> $b->end } @mirnas) {
		my ($mA) = $ma->get_tag_values("accession");
		$mA	=~  s/\s+//g;
		next if ($seen{$mA});
		my ($mB,$mb);
		my $MB;
		foreach $mb (sort { $a->start <=> $b->end } @mirnas) {
			($mB) = $mb->get_tag_values("accession");
			$mB	=~  s/\s+//g;
			next if ($mA eq $mB);
			next if ((exists $seen{$mA} && exists $seen{$mA}{$mB}) || (exists $seen{$mB} && exists $seen{$mB}{$mA}));
			next if ($ma->overlaps($mb));
			if (is_pairing($mA,$mB,$ma,$mb,$matrix,$sequence,\$fold,\$energy,\$matrix,\$lucky)){
				if (analyze_miRNA_pair($mA,$mB,$ma,$mb,$sequence,$fold,$length,$matrix,\%pairs,\%seqs)){
					$MB=$mb;
					$seen{$mA}{$mB}++;
					$seen{$mB}{$mA}++;
					last;
				}
			}
			$seen{$mA}{$mB}++;
			$seen{$mB}{$mA}++;
		}
		my ($construct_seq);
		unless ($pairs{$mA} and $MB){
			next unless (construct_missing($mA,$ma,\$mb_ref,$length,$matrix,$s,\$construct_seq));
			$mb=$$mb_ref;
			($mB) = $mb->get_tag_values("accession");
			$mB	=~  s/\s+//g;
			next unless (analyze_miRNA_pair($mA,$mB,$ma,$mb,$sequence,$fold,$length,$matrix,\%pairs,\%seqs));
			$MB=$mb;
			my $t=$MB->get_tag_values("accession");
			$t=~  s/\s+//g;
			$MB->display_name($t);
		}
		if ($pairs{$mA} and $pairs{$mB} and $ma and $MB){
			if (defined $training){
				print T join(",",format_pair($ma,$MB,$mA,$mB,$sequence,$fold,\%pairs,\%seqs,$matrix,$length),compSEQ_pair($ma->seq,$construct_seq && $construct_seq->isa("Bio::SeqI") ? $construct_seq : $MB->seq,$randfile2,$randfile3),$energy,$lucky,"Y"),"\n";
				print B "$mA\t$mB\t$sequence\t$fold\t$energy\n";
				print G format_gff($ma,$MB,$mA,$mB,$s,$source,$count),"\n" unless (defined $training);
				
			}
			else {
				print T join(",",format_pair($ma,$MB,$mA,$mB,$sequence,$fold,\%pairs,\%seqs,$matrix,$length),compSEQ_pair($ma->seq,$construct_seq && $construct_seq->isa("Bio::SeqI") ? $construct_seq : $MB->seq,$randfile2,$randfile3),$energy,$lucky,"?"),"\n";
				print B "$mA\t$mB\t$sequence\t$fold\t$energy\n";
				print G format_gff($ma,$MB,$mA,$mB,$s,$source,$count),"\n" unless (defined $training);
			}
			my $i=0;
			if (defined $training){
				foreach my $rs ( @$randseqs) {
					$i++;
					my $rmA		= join(".", $mA, "random",$i);
					my $rmB		= join(".", $mB, "random",$i);
					my $rma_seq	= Bio::Seq->new(-display_id=>$rmA, -seq=>$rs->subseq($ma->start,$ma->end));
					my $rma		= Bio::SeqFeature::Generic->new(-display_name=>$rmA, -primary_tag=>"miRNA", -start=>$ma->start ,-end=> $ma->end,  -tag => { accession => $rmA});
					my $rmb_seq	= Bio::Seq->new(-display_id=>$rmB, -seq=>$rs->subseq($MB->start,$MB->end));
					my $rmb		= Bio::SeqFeature::Generic->new(-display_name=>$rmB, -primbry_tag=>"miRNA", -start=>$MB->start ,-end=> $MB->end,  -tag => { accession => $rmB});
					my ($rfold) 	= RNAfold($rs->seq);
					my ($rmatrix) 	= RNAeval($rs->seq,$rfold);
					my $rlength	= $rs->length();
					my %rpairs;
					my %rseqs;
					next unless (analyze_miRNA_pair($rmA,$rmB,$rma,$rmb,$rs->seq,$rfold,$rlength,$rmatrix,\%rpairs,\%rseqs));
					print T join(",",format_pair($rma,$rmb,$rmA,$rmB,$rs->seq,$rfold,\%rpairs,\%rseqs,$rmatrix,$length),compSEQ_pair($rma_seq,$rmb_seq,$randfile2,$randfile3),$energy,$lucky,"N"),"\n";
				}
			}
		}
		$count++;
		undef %pairs;
	}
}
close T;
close B;
close G;

sub RNAfold {
        my ($pre_seq) = @_;
        my ($seq,$brac,$energy);
        open(R,"echo $pre_seq | RNAfold --noPS |");# COMPUTE01 VERSION
#        open(R,"echo $pre_seq | RNAfold -noPS |"); # CLUSTER VERSION
        while(my $r=<R>){
                chomp $r;
                if ($r=~/^([gautcGAUTC]+)$/){
                        $seq=$1;
                        $seq=~tr/gaucGAUC/GATCGATC/;
                }
		if ($r=~/^([().]+)\s+\(\s*(-?\d+.\d+)\)/){
			$brac=$1;
			$energy=$2;
		}
        }
        close R;
        return ($brac,$energy);
}

sub RNAeval {
        my ($seq,$brac) = @_;
        my $in = join("\n",$seq,$brac);
#        open (T,">tmp.txt");
	my $f = File::Temp->new(TEMPLATE=>"RNAeval.XXXXXXX",DIR=>$tempdir,UNLINK=>0,SUFFIX=>".out");
        print $f "$in\n";
        open(E,"cat ".$f->filename." | RNAeval -v -d2 |");
        my @matrix;
        while(my $e=<E>){
                chomp $e;
                if ($e=~/Interior\s+loop\s*+\(\s*(\d+),\s*(\d+)\)\s*\w+;\s*\(\s*\d+,\s*\d+\)\s*\w+:\s*\-?\d+/){
                        $matrix[$1]=$2;
                        $matrix[$2]=$1;
                        if ($3 and $4){
                                $matrix[$3]=$4;
                                $matrix[$4]=$3;
                        }
                }
        }
        close E;
        return (\@matrix);
}

sub is_pairing {
        my ($m_A,$m_B,$m_a,$m_b,$matrix,$sequence,$bracket_ref,$energy_ref,$matrix_ref,$lucky) = @_;
        my ($m_five,$m_three,$five_s,$five_e,$three_s,$three_e,$m_FIVE,$m_THREE);
        if ($m_a->start()<$m_b->start()){
                $five_s=$m_a->start();
                $five_e=$m_a->end();
                $three_s=$m_b->start();
                $three_e=$m_b->end();
                $m_five=$m_a;
                $m_three=$m_b;
                $m_FIVE=$m_A;
                $m_THREE=$m_B;
        }
        else {
                $five_s=$m_b->start();
                $five_e=$m_b->end();
                $three_s=$m_a->start();
                $three_e=$m_a->end();
                $m_five=$m_b;
                $m_three=$m_a;
                $m_FIVE=$m_B;
                $m_THREE=$m_A;
        }
        my $match=0;
        for (my $i=$five_s; $i<=$five_e; $i++){
                if ($$matrix[$i] and $$matrix[$i]<=$three_e and $$matrix[$i]>=$three_s){
                        $match++;
                }
        }
        if ($match >= 7){
                return 1;
        }
        else {
		my $bracket;
		my $last_match;
		my $bracket_tmp;
		my $energy_tmp;
		my @matrix;
		my $energy;
		for (my $i=1; $i<=100; $i++){
			open(R,"echo $sequence | RNAfold --ImFeelingLucky --noPS |");# COMPUTE01 VERSION
#		        open(R,"echo $sequence | RNAfold --ImFeelingLucky -noPS |"); # CLUSTER VERSION
				while(my $r=<R>){
					chomp $r;
					if ($r=~/^([().]+)\s+\(\s*(-?\d+.\d+)\)/){
						$bracket_tmp=$1;
						$energy_tmp=$2;
					}
				}
			close R;
			my $in = join("\n",$sequence,$bracket_tmp);
			my $f = File::Temp->new(TEMPLATE=>"RNAeval.XXXXXXX",DIR=>$tempdir,UNLINK=>0,SUFFIX=>".out");
			print $f "$in\n";
			open(E,"cat ".$f->filename." | RNAeval -v -d2 |");
			my @matrix_tmp;
			while(my $e=<E>){
				chomp $e;
				if ($e=~/Interior\s+loop\s*+\(\s*(\d+),\s*(\d+)\)\s*\w+;\s*\(\s*\d+,\s*\d+\)\s*\w+:\s*\-?\d+/){
					$matrix_tmp[$1]=$2;
					$matrix_tmp[$2]=$1;
					if ($3 and $4){
						$matrix_tmp[$3]=$4;
						$matrix_tmp[$4]=$3;
					}
				}
			}
			close E;
			for (my $i=$five_s; $i<=$five_e; $i++){
				if ($matrix_tmp[$i] and $matrix_tmp[$i]<=$three_e and $matrix_tmp[$i]>=$three_s){
					$match++;
				}
			}
			if ($last_match and $match>=$last_match){
		        	@matrix=@matrix_tmp;
				$bracket=$bracket_tmp;
				$energy=$energy_tmp;
        		}
			$last_match=$match;
		}
		if ($match >= 7){
			$matrix_ref=\@matrix;
			$bracket_ref=\$bracket;
			$energy_ref=\$energy;
			$lucky="Y";
			return 1;
		}
		else {
			return 0;
		}
        }
}

sub analyze_miRNA_pair {
        my ($m_A,$m_B,$m_a,$m_b,$seq,$bracs,$pre_l,$pos,$pairs,$seqs)=@_;
        my ($m_five,$m_three,$five_s,$five_e,$three_s,$three_e,$m_FIVE,$m_THREE);
        if ($m_a->start()<$m_b->start()){
                $five_s=$m_a->start();
                $five_e=$m_a->end();
                $three_s=$m_b->start();
                $three_e=$m_b->end();
                $m_five=$m_a;
                $m_three=$m_b;
                $m_FIVE=$m_A;
                $m_THREE=$m_B;
        }
        else {
                $five_s=$m_b->start();
                $five_e=$m_b->end();
                $three_s=$m_a->start();
                $three_e=$m_a->end();
                $m_five=$m_b;
                $m_three=$m_a;
                $m_FIVE=$m_B;
                $m_THREE=$m_A;
        }
	my ($base,$start,$stop,$k);
        my %all_bases;
        my ($to_find,$dist_start,$overhang_five,$overhang_three);
        my (@five_bracs,@five_seq,@three_bracs,@three_seq,$bracdot);
        my @brackets=split("",$bracs);
        my @sequence=split("",$seq);
        for (my $i=$five_e; $i>$five_e-50; $i--){
                if ($i<=0){
                        push(@five_seq,"?");
                        push(@five_bracs,"?");
                }
                else {
                        if($brackets[$i-1] eq "("){
                                $bracdot = "LB";
                        }
                        elsif($brackets[$i-1] eq ")"){
                                $bracdot = "RB";
                        }
                        elsif($brackets[$i-1] eq "."){
                                $bracdot = "DT";
                        }
                        push(@five_seq,$sequence[$i-1]);
                        push(@five_bracs,$bracdot);
                }
        }
        for (my $i=$three_s; $i<$three_s+50; $i++){
                if ($i>$pre_l){
                        push(@three_seq,"?");
                        push(@three_bracs,"?");
                }
                else {
                        if($brackets[$i-1] eq "("){
                                $bracdot = "LB";
                        }
                        elsif($brackets[$i-1] eq ")"){
                                $bracdot = "RB";
                        }
                        elsif($brackets[$i-1] eq "."){
                                $bracdot = "DT";
                        }
                        push(@three_seq,$sequence[$i-1]);
                        push(@three_bracs,$bracdot);
                }
        }
	@{$$pairs{$m_THREE}}=@three_bracs;
        @{$$pairs{$m_FIVE}}=@five_bracs;
        @{$$seqs{$m_THREE}}=@three_seq;
        @{$$seqs{$m_FIVE}}=@five_seq;
        if ($$pairs{$m_FIVE} and $$pairs{$m_THREE} and $$seqs{$m_THREE} and $$seqs{$m_FIVE}){
                return 1;
        }
        else {
                return 0;
        }
}

sub construct_missing {
        my ($m_A,$m_a,$mb_ref,$pre_l,$pos,$seq, $SEQ)=@_;
        my $start=$m_a->start();
        my $stop=$m_a->end();
        my ($to_find,$dist_start,$dist_stop);
	my $con_start=0;
	my $con_stop=$pre_l;
        for (my $i=$start; $i<=$stop; $i++){
                if ($$pos[$i]){
                        $to_find=$i;
                        $dist_start=$i-$start;
                        $con_stop=$$pos[$i]+$dist_start+2;
                        if ($con_stop>=$pre_l){
                                $con_stop=$pre_l;
                        }
                        last;
                }
        }
        for (my $i=$stop; $i>=$start; $i--){
                if ($$pos[$i]){
                        $to_find=$i;
                        $dist_stop=$stop-$i;
                        $con_start=$$pos[$i]-$dist_stop+2;
                        if ($con_start>=$pre_l){
                                $con_start=$pre_l;
                        }
                        last;
                }
        }
        if ($con_start>=$con_stop or $con_stop-$con_start<=0 or $con_start<=0 or $con_stop>=$pre_l){
                return 0;
        }
        else {
                my $m_B=join("-",$m_A,"constructed");
		$$SEQ = Bio::Seq->new(-display_id=>$m_B, -seq=>$seq->subseq($con_start,$con_stop));
                my $construct = Bio::SeqFeature::Generic->new(-display_name=>$m_B, -start => $con_start, -end => $con_stop, -primary => 'miRNA', -tag => { accession => $m_B});
                $$mb_ref=\$construct;
                return 1;
        }
}

sub format_pair {
        my ($m_a,$m_b,$mA,$mB,$seq,$bracs,$pair_bracs,$pair_seqs,$matrix,$pre_l)=@_;
        my ($five_s,$five_e,$three_s,$three_e,$m_five,$m_three,$m_FIVE,$m_THREE);
        if ($m_a->start()<$m_b->start()){
                $five_s=$m_a->start();
                $five_e=$m_a->end();
                $three_s=$m_b->start();
                $three_e=$m_b->end();
                $m_five=$m_a;
                $m_three=$m_b;
                $m_FIVE=$mA;
                $m_THREE=$mB;
        }
        else {
                $five_s=$m_b->start();
                $five_e=$m_b->end();
                $three_s=$m_a->start();
                $three_e=$m_a->end();
                $m_five=$m_b;
                $m_three=$m_a;
                $m_FIVE=$mB;
                $m_THREE=$mA;
        }
        # 1st, 2nd id
        my $formatted = join("::",$m_FIVE,$m_THREE);

        # 1st structure
        my $five_bracs = join(",",@{$pair_bracs->{$m_FIVE}});
        $formatted = join(",",$formatted,$five_bracs);

        # 1st structure fraction
        my $dots5 = grep(/DT/,@{$pair_bracs->{$m_FIVE}});
        my $left5 = grep(/LB/,@{$pair_bracs->{$m_FIVE}});
        my $right5 = grep(/RB/,@{$pair_bracs->{$m_FIVE}});
        my $na5 = grep(/\?/,@{$pair_bracs->{$m_FIVE}});
        my $frac_dot5=$dots5/50;
        my $frac_left5=$left5/50;
        my $frac_right5=$right5/50;
        my $frac_na5=$na5/50;
        $formatted=join(",",$formatted,$frac_dot5,$frac_left5,$frac_right5,$frac_na5);

        # 2nd structure
        my $three_bracs = join(",",@{$pair_bracs->{$m_THREE}});
        $formatted = join(",",$formatted,$three_bracs);

        # 2nd structure fraction
        my $dots3 = grep(/DT/,@{$pair_bracs->{$m_THREE}});
        my $left3 = grep(/LB/,@{$pair_bracs->{$m_THREE}});
        my $right3 = grep(/RB/,@{$pair_bracs->{$m_THREE}});
        my $na3 = grep(/\?/,@{$pair_bracs->{$m_THREE}});
        my $frac_dot3=$dots3/50;
        my $frac_left3=$left3/50;
        my $frac_right3=$right3/50;
        my $frac_na3=$na3/50;
        $formatted=join(",",$formatted,$frac_dot3,$frac_left3,$frac_right3,$frac_na3);
	
	# 1st bases
        my $five_bases = join(",",@{$pair_seqs->{$m_FIVE}}) if $pair_seqs->{$m_FIVE};
	$formatted = join(",",$formatted,$five_bases);

        # 1st bases fraction
        my $A5 = grep(/A/,@{$pair_seqs->{$m_FIVE}});
        my $T5 = grep(/T/,@{$pair_seqs->{$m_FIVE}});
        my $G5 = grep(/G/,@{$pair_seqs->{$m_FIVE}});
        my $C5 = grep(/C/,@{$pair_seqs->{$m_FIVE}});
        my $N5 = grep(/N/,@{$pair_seqs->{$m_FIVE}});
        my $nb5 = grep(/\?/,@{$pair_seqs->{$m_FIVE}});
        my $frac_a5=$A5/50;
        my $frac_t5=$T5/50;
        my $frac_g5=$G5/50;
        my $frac_c5=$G5/50;
        my $frac_n5=$N5/50;
        my $frac_nb5=$nb5/50;
        $formatted=join(",",$formatted,$frac_a5,$frac_t5,$frac_g5,$frac_c5,$frac_n5,$frac_nb5);

        # 2nd bases
        my $three_bases = join(",",@{$pair_seqs->{$m_THREE}});
        $formatted = join(",",$formatted,$three_bases);

        # 2nd bases fraction
        my $A3 = grep(/A/,@{$pair_seqs->{$m_THREE}});
        my $T3 = grep(/T/,@{$pair_seqs->{$m_THREE}});
        my $G3 = grep(/G/,@{$pair_seqs->{$m_THREE}});
        my $C3 = grep(/C/,@{$pair_seqs->{$m_THREE}});
        my $N3 = grep(/N/,@{$pair_seqs->{$m_THREE}});
        my $nb3 = grep(/\?/,@{$pair_seqs->{$m_THREE}});
        my $frac_a3=$A3/50;
        my $frac_t3=$T3/50;
        my $frac_g3=$G3/50;
        my $frac_c3=$G3/50;
        my $frac_n3=$N3/50;
        my $frac_nb3=$nb3/50;
	$formatted=join(",",$formatted,$frac_a3,$frac_t3,$frac_g3,$frac_c3,$frac_n3,$frac_nb3);

        die unless (scalar(@{$pair_bracs->{$m_FIVE}})==50 or scalar(@{$pair_bracs->{$m_THREE}})==50 or scalar(@{$pair_seqs->{$m_FIVE}})==50 or scalar(@{$pair_seqs->{$m_THREE}})==50);
	my $loop_length=($three_s-1)-($five_e+1)+1;
        my ($frac_dot_loop,$frac_left_loop,$frac_right_loop,$frac_na_loop,$frac_A_loop,$frac_T_loop,$frac_G_loop,$frac_C_loop,$frac_N_loop,$frac_nb_loop);
        if ($loop_length<=0){
                $frac_dot_loop="?";
                $frac_left_loop="?";
                $frac_right_loop="?";
                $frac_na_loop="?";
                $frac_A_loop="?";
                $frac_T_loop="?";
                $frac_G_loop="?";
                $frac_C_loop="?";
                $frac_N_loop="?";
                $frac_nb_loop="?";
        }
	else {
                my @brackets=split("",$bracs);
                my $dotsloop=0;
                my $leftloop=0;
                my $rightloop=0;
                my $naloop=0;
                for (my $i=$five_e+1; $i<=$three_s-1; $i++){
                        if ($i>$pre_l){
                                $naloop++;
                        }
                        else {
                                if($brackets[$i-1] eq "("){
                                        $leftloop++;
                                }
                                elsif($brackets[$i-1] eq ")"){
                                        $rightloop++;
                                }
                                elsif($brackets[$i-1] eq "."){
                                        $dotsloop++;
                                }
                        }
                }
                $frac_dot_loop=$dotsloop/$loop_length;
                $frac_left_loop=$leftloop/$loop_length;
                $frac_right_loop=$rightloop/$loop_length;
                $frac_na_loop=$naloop/$loop_length;
                my @sequence=split("",$seq);
                my $Al=0;
                my $Tl=0;
                my $Gl=0;
                my $Cl=0;
                my $Nl=0;
                my $nbl=0;
                for (my $i=$five_e+1; $i<=$three_s-1; $i++){
                        if ($i>$pre_l){
                                $nbl++;
                        }
                        else {
                                if($sequence[$i-1] eq "A"){
                                        $Al++;
                                }
                                elsif($sequence[$i-1] eq "T"){
                                        $Tl++;
                                }
                                elsif($sequence[$i-1] eq "G"){
                                        $Gl++;
                                }
                                elsif($sequence[$i-1] eq "C"){
                                        $Cl++;
                                }
                                elsif($sequence[$i-1] eq "N"){
                                        $Nl++;
                                }
                        }
                }
		$frac_A_loop=$Al/$loop_length;
                $frac_T_loop=$Tl/$loop_length;
                $frac_G_loop=$Gl/$loop_length;
                $frac_C_loop=$Cl/$loop_length;
                $frac_N_loop=$Nl/$loop_length;
                $frac_nb_loop=$nbl/$loop_length;
        }
        $formatted=join(",",$formatted,$frac_dot_loop,$frac_left_loop,$frac_right_loop,$frac_na_loop);
        $formatted=join(",",$formatted,$frac_A_loop,$frac_T_loop,$frac_G_loop,$frac_C_loop,$frac_N_loop,$frac_nb_loop);
        my $fields = () = $formatted =~ /[^,]+/gi;
        #die "Wrong field count: $fields != 226\n" unless ($fields==226);
	return ($formatted);
        #print "$formatted\n";
}

sub compSEQ_pair {
	my ($MA,$MB,$randfile2,$randfile3) = @_;
	return (join(",", compSEQ($MA,2,$randfile2,\@dinuc), compSEQ($MA,3,$randfile3,\@trinuc),compSEQ($MB,2,$randfile2,\@dinuc), compSEQ($MB,3,$randfile3,\@trinuc)));
}

sub compSEQ_random {
	my ($seq,$times) = @_;
	my @s = split '', $seq->seq;
	my @rand;
	
	foreach my $i (1 .. $times) {
		@s		= shuffle @s;
		my $s	= Bio::Seq->new(-id=> join(".", $seq->id, "random",$i), -seq=> join("", @s));
		push @rand, $s;
	}
	my $f = File::Temp->new(TEMPLATE=>"RANDOM2.XXXXXXX",DIR=>$tempdir,UNLINK=>0,SUFFIX=>"compseq.out");
	$compseq->run({-word=>2, -outfile=>$f->filename, -sequence=>\@rand});
	my $f3 = File::Temp->new(TEMPLATE=>"RANDOM.XXXXXXX",DIR=>$tempdir,UNLINK=>0,SUFFIX=>"compseq.out");
	$compseq->run({-word=>3, -outfile=>$f3->filename, -sequence=>\@rand});

	return($f->filename, $f3->filename, \@rand);
}


sub compSEQ {
	my ($seq,$word,$random, $nuc) = @_;
	my $f 	= File::Temp->new(TEMPLATE=>"SEQ.XXXXXXX",DIR=>$tempdir,UNLINK=>0,SUFFIX=>"compseq.out");
	$compseq->run({-word=>$word, -outfile=>$f->filename, -sequence=>$seq,-infile=>$random});
	my ($freq,$ratio) = parse_compseq($f->filename);
	return(join(",",map({ $freq->{$_} ? $freq->{$_} : "?" } @$nuc), map({ $ratio->{$_} ? $ratio->{$_} : "?" } @$nuc)));
}

sub parse_compseq {
	my ($file) = @_;

	open(F, $file) or die "Cannot open compseq result $file! $!\n";
	my (%freq,%ratio);
	while (<F>) {
		if (/^([ACTGN]+)\t+([0-9\.\-eE]+)\t+([0-9\.\-eE]+)\t+([0-9\.\-eE]+)\t+([0-9\.\-eE]+)$/) {
			$freq{$1}=$3;
			$ratio{$1}=$5;
		}
	}
	close(F);
	return(\%freq,\%ratio);
}

sub format_gff {
	my ($ma,$mb,$MA,$MB,$s,$source,$i) = @_;
	my ($tool);
	if ($ma->has_tag("evidence")){
		($tool) = $ma->get_tag_values("evidence");
	}
	elsif ($mb->has_tag("evidence")){
		($tool) = $mb->get_tag_values("evidence");
	}
	else {
		$tool = "";
	}
	my $ref_seq	= $s->seq();
	my $length 	= $s->length();
	my ($ref)	= $source->get_tag_values("source_seq_id");
	my ($strand)	= $source->get_tag_values("source_strand");
	my ($pri_start) = $source->get_tag_values("source_start");
	my ($pri_end) = $source->get_tag_values("source_end");
	my $pri_id	= $s->accession();

	my $pri_as_ref 	= Bio::Location::Simple->new( -seq_id => $pri_id, -start => 1, -end => (1+$length-1), -strand => +1 );
        my $pri_on_ref 	= Bio::Location::Simple->new( -seq_id => $ref, -start => $pri_start, -end => $pri_end, -strand => $strand );
	my $pri_to_ref 	= Bio::Coordinate::Pair->new( -in  => $pri_as_ref, -out => $pri_on_ref);

	my $mir_a_id	= $MA;
	my $mir_a_start	= $ma->start();
	my $mir_a_end	= $ma->end();
        my $mir_a_on_pri= Bio::Location::Simple->new( -seq_id => $mir_a_id, -start => $mir_a_start, -end => $mir_a_end, -strand => +1 );
        my $mir_a_on_ref= $pri_to_ref->map( $mir_a_on_pri );
	
	my $mir_b_id	= $MB;
	my $mir_b_start	= $mb->start();
	my $mir_b_end	= $mb->end();
	my $mir_b_on_pri= Bio::Location::Simple->new( -seq_id => $mir_b_id, -start => $mir_b_start, -end => $mir_b_end, -strand => +1 );
        my $mir_b_on_ref= $pri_to_ref->map( $mir_b_on_pri );
	
	my ($mir5_start,$mir5_end,$mir5_id,$mir3_start,$mir3_end,$mir3_id);

	if ($mir_a_on_ref->start <= $mir_b_on_ref->start){
		$mir5_start	= $mir_a_on_ref->start;
		$mir5_end	= $mir_a_on_ref->end;
        	$mir5_id	= $MA;
		$mir3_start	= $mir_b_on_ref->start;
                $mir3_end	= $mir_b_on_ref->end;
                $mir3_id	= $MB;
	}
	else {
		$mir3_start	= $mir_a_on_ref->start;
                $mir3_end	= $mir_a_on_ref->end;
                $mir3_id	= $MA;
                $mir5_start	= $mir_b_on_ref->start;
                $mir5_end	= $mir_b_on_ref->end;
                $mir5_id	= $MB;
	}
	my $print_strand;
	if ($strand eq "+1"){
		$print_strand	= "+";
	}
	else {
		$print_strand	= "-";
	}
	my $pre_start	= $mir5_start;
	my $pre_end	= $mir3_end;
	$i = sprintf("%06d", $i);
	my $pre_id	= join(":",$pri_id,$i);
	my $pri_gff 	= join("\t",$ref,$tool,"miRNA_primary_transcript",$pri_start,$pri_end,".",$print_strand,".","ID:$pri_id");
	my $pre_gff 	= join("\t",$ref,$tool,"pre_miRNA",$pre_start,$pre_end,".",$print_strand,".","ID:$pre_id;Parent:$pri_id");
	my $mir5_gff 	= join("\t",$ref,$tool,"miRNA",$mir5_start,$mir5_end,".",$print_strand,".","ID:$mir5_id;Derives_from:$pri_id");
	my $mir3_gff 	= join("\t",$ref,$tool,"miRNA",$mir3_start,$mir3_end,".",$print_strand,".","ID:$mir3_id;Derives_from:$pri_id");
	my $gff3	= join("\n",$pri_gff,$pre_gff,$mir5_gff,$mir3_gff);
	return ($gff3);
}
